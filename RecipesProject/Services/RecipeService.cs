﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RecipesProject.Entities;
using RecipesProject.Exceptions;
using RecipesProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecipesProject.Services
{
    public interface IRecipeService
    {
        int CreateRecipe(CreateRecipeDto recipeParam);
        void Delete(int id);
        void EditRecipe(int id, CreateRecipeDto recipeParam);
        List<RecipeDto> GetAll();
        RecipeDto GetById(int id);
    }
    public class RecipeService : IRecipeService
    {
        private readonly RecipesDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<RecipeService> _logger;

        public RecipeService(RecipesDbContext context, IMapper mapper,ILogger<RecipeService> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public List<RecipeDto> GetAll()
        {
            var recipes = _context
                .Recipes
                .Include(c => c.Category)
                .Include(a => a.Author)
                .ToList();

            if (recipes is null)
                throw new NotFoundException("Recipe not found");

            var result = _mapper.Map<List<RecipeDto>>(recipes);
            return result;
        }


        public RecipeDto GetById(int id)
        {
            var recipe = _context
                .Recipes
                .Include(c => c.Category)
                .Include(a => a.Author)
                .FirstOrDefault(r => r.Id == id);

            if (recipe is null) 
                throw new NotFoundException("Recipe not found");

            var result = _mapper.Map<RecipeDto>(recipe);

            return result;
        }

        public int CreateRecipe(CreateRecipeDto recipeParam)
        {
            Recipe recipe = _mapper.Map<Recipe>(recipeParam);

            recipe.Category = _context.Categories.FirstOrDefault(c => c.Id == recipeParam.CategoryId);
            recipe.Author = _context.Authors.FirstOrDefault(c => c.Id == recipeParam.AuthorId);

            _context.Add(recipe);
            _context.SaveChanges();

            _logger.LogInformation($"CREATE recipe action with id:{recipe.Id}");

            return recipe.Id;
        }

        public void EditRecipe(int id, CreateRecipeDto recipeParam)
        {

            var recipe = _context.Recipes.FirstOrDefault(r => r.Id == id);

            if(recipe is null) 
                throw new NotFoundException("Recipe not found");

            recipe =_mapper.Map<Recipe>(recipeParam);

            recipe.Category = _context.Categories.FirstOrDefault(c => c.Id == recipeParam.CategoryId);
            recipe.Author = _context.Authors.FirstOrDefault(c => c.Id == recipeParam.AuthorId);

            _context.Update(recipe);
            _context.SaveChanges();

            _logger.LogInformation($"UPDATE action for id:{recipe.Id}");

        }

        public void Delete(int id)
        {
            var recipe = _context.Recipes.FirstOrDefault(r => r.Id == id);

            if (recipe is null) 
                throw new NotFoundException("Recipe not found");

            _context.Remove<Recipe>(recipe);
            var result = _context.SaveChanges();

            _logger.LogWarning($"DELETE action for id:{recipe.Id}");

        }
    }
}
