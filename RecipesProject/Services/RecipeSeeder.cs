﻿using RecipesProject.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RecipesProject.Enums;

namespace RecipesProject.Services
{
    public class RecipeSeeder
    {
        private readonly RecipesDbContext _dbContext;
        public RecipeSeeder(RecipesDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Seed()
        {
            if (_dbContext.Database.CanConnect())
            {
                if (!_dbContext.Authors.Any())
                {
                    var authorToDb = new Author()
                    {
                        Name = "Admin"
                    };

                    _dbContext.AddRange(authorToDb);
                    _dbContext.SaveChanges();
                }

                if (!_dbContext.Recipes.Any())
                {
                    #region declaringValues

                    var defaultAuthor = _dbContext.Authors.FirstOrDefault().Id;

                    var recipesToDB = new List<Category>() {
                        new Category() { Name = "Obiad" ,
                            Recipes = new List<Recipe>(){
                            new Recipe(){
                                Difficulty = Difficulty.Easy,
                                Portions= 3,
                                Title = "Spaghetti",
                                MakingTime = "2 godziny",
                                Description = "Opis krok po kroku",
                                Ingrediens = "1 opakowanie tego, 1 puszka tamtego, szczypta owego, 30 ml siamtego",
                                AuthorId = defaultAuthor
                                
                            },
                                new Recipe(){
                                Difficulty = Difficulty.Easy,
                                Portions= 3,
                                Title = "Schabowe",
                                MakingTime = "2 godziny",
                                Description = "Opis krok po kroku",
                                Ingrediens = "1 opakowanie tego, 1 puszka tamtego, szczypta owego, 30 ml siamtego",
                                AuthorId = defaultAuthor
                            },
                            new Recipe()
                            {
                                Difficulty = Difficulty.Medium,
                                Portions = 2,
                                Title = "Schab parzony",
                                MakingTime = "2,5 godziny",
                                Description = "Opis krok po kroku",
                                Ingrediens = "1 opakowanie tego, 1 puszka tamtego, szczypta owego, 30 ml siamtego",
                                AuthorId = defaultAuthor
                            },
                                new Recipe()
                            {
                                Difficulty = Difficulty.Hard,
                                Portions = 2,
                                Title = "Łosoś sou'vide",
                                MakingTime = "1,5 godziny",
                                Description = "Opis krok po kroku",
                                Ingrediens = "1 opakowanie tego, 1 puszka tamtego, szczypta owego, 30 ml siamtego",
                                AuthorId = defaultAuthor
                            }
                            }
                        },
                        new Category() { Name = "Deser" ,
                        Recipes = new List<Recipe>()
                        {

                            new Recipe()
                            {
                                Difficulty = Difficulty.Medium,
                                Portions = 2,
                                Title = "Mus czekoladowy",
                                MakingTime = "1 godzina",
                                Description = "Opis krok po kroku",
                                Ingrediens = "1 opakowanie tego, 1 puszka tamtego, szczypta owego, 30 ml siamtego",
                                AuthorId = defaultAuthor
                            },
                                new Recipe()
                            {
                                Difficulty = Difficulty.Hard,
                                Portions = 2,
                                Title = "Tort bezowy",
                                MakingTime = "5 godzin",
                                Description = "Opis krok po kroku",
                                Ingrediens = "1 opakowanie tego, 1 puszka tamtego, szczypta owego, 30 ml siamtego",
                                AuthorId = defaultAuthor
                            }
                        }

                         },
                        new Category() { Name = "Przekąska",
                        Recipes = new List<Recipe>(){

                            new Recipe()
                            {
                                Difficulty = Difficulty.Medium,
                                Portions = 5,
                                Title = "Skrzydełka z kurczaka w miodzie",
                                MakingTime = "1,5 godziny",
                                Description = "Opis krok po kroku",
                                Ingrediens = "1 opakowanie tego, 1 puszka tamtego, szczypta owego, 30 ml siamtego",
                                AuthorId = defaultAuthor
                            },
                                new Recipe()
                            {
                                Difficulty = Difficulty.Easy,
                                Portions = 5,
                                Title = "Krakersy serowe",
                                MakingTime = "1,5 godziny",
                                Description = "Opis krok po kroku",
                                Ingrediens = "1 opakowanie tego, 1 puszka tamtego, szczypta owego, 30 ml siamtego",
                                AuthorId = defaultAuthor
                            }} },

                        new Category(){ Name = "Śniadanie",
                        Recipes = new List<Recipe>(){
                        new Recipe()
                            {
                                Difficulty = Difficulty.Easy,
                                Portions = 2,
                                Title = "Jajecznica",
                                MakingTime = "30 minut",
                                Description = "Opis krok po kroku",
                                Ingrediens = "1 opakowanie tego, 1 puszka tamtego, szczypta owego, 30 ml siamtego",
                                AuthorId = defaultAuthor
                            },
                                new Recipe()
                            {
                                Difficulty = Difficulty.Easy,
                                Portions = 2,
                                Title = "Kanapki",
                                MakingTime = "30 minut",
                                Description = "Opis krok po kroku",
                                Ingrediens = "1 opakowanie tego, 1 puszka tamtego, szczypta owego, 30 ml siamtego",
                                AuthorId = defaultAuthor
                            }} },
                        new Category(){Name = "Kolacja"
                        ,
                        Recipes = new List<Recipe>(){
                        new Recipe()
                            {
                                Difficulty = Difficulty.Easy,
                                Portions = 2,
                                Title = "Parówki",
                                MakingTime = "10 minut",
                                Description = "Opis krok po kroku",
                                Ingrediens = "1 opakowanie tego, 1 puszka tamtego, szczypta owego, 30 ml siamtego",
                                AuthorId = defaultAuthor
                            },
                                new Recipe()
                            {
                                Difficulty = Difficulty.Easy,
                                Portions = 2,
                                Title = "Kasza manna na mleku",
                                MakingTime = "15 minut",
                                Description = "Opis krok po kroku",
                                Ingrediens = "1 opakowanie tego, 1 puszka tamtego, szczypta owego, 30 ml siamtego",
                                AuthorId = defaultAuthor
                            }} }
                    };
                    #endregion

                    _dbContext.AddRange(recipesToDB);
                    _dbContext.SaveChanges();

                }
            }
        }


    }
}
