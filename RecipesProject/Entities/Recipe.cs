﻿using RecipesProject.Enums;
using System.ComponentModel.DataAnnotations;

namespace RecipesProject.Entities
{
    public class Recipe
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }
        public Difficulty Difficulty { get; set; }
        public string MakingTime { get; set; }
        public int Portions { get; set; }
        public string Description { get; set; }
        public string Ingrediens { get; set; }
        [Required]
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }
        [Required]
        public int AuthorId { get; set; }
        public virtual Author Author { get; set; }


    }
}
