﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecipesProject.Entities
{
    public class RecipesDbContext : DbContext
    {
        //    private string _connectionString = "Server=localhost;Database=master;Trusted_Connection=True;";
        public DbSet<Category> Categories { get; set; }
        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<Author> Authors { get; set; }

        public RecipesDbContext(DbContextOptions dbContextOptions) : base(dbContextOptions)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Recipe>()
                .Property(r => r.Title)
                .IsRequired()
                .HasMaxLength(50);

            modelBuilder.Entity<Recipe>()
                .Property(r => r.AuthorId)
                .IsRequired()
                .HasDefaultValue(1);

            modelBuilder.Entity<Recipe>()
                .Property(r => r.CategoryId)
                .IsRequired();

            modelBuilder.Entity<Author>()
                .Property(a => a.Name)
                .IsRequired();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // optionsBuilder.UseSqlServer(_connectionString);
        }
    }
}
