﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecipesProject.Models
{
    public class RecipeDto
    {
        public string Title { get; set; }
        public int Difficulty { get; set; }
        public string MakingTime { get; set; }
        public int Portions { get; set; }
        public string Description { get; set; }
        public string Ingrediens { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int AuthorId { get; set; }
        public string AuthorName { get; set; }
    }
}
