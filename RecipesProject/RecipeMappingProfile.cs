﻿using AutoMapper;
using RecipesProject.Entities;
using RecipesProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecipesProject
{
    public class RecipeMappingProfile : Profile
    {
        public RecipeMappingProfile()
        {
            CreateMap<Recipe, RecipeDto>()
                .ForMember(m => m.CategoryName, cn => cn.MapFrom(c => c.Category.Name))
                .ForMember(m => m.AuthorName, an => an.MapFrom(a => a.Author.Name));

            CreateMap<CreateRecipeDto, Recipe>();

            CreateMap<Category, CategoryDto>().ReverseMap();

            CreateMap<Author, AuthorDto>().ReverseMap();
        }
    }
}
