﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RecipesProject.Entities;
using RecipesProject.Enums;
using RecipesProject.Models;
using RecipesProject.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecipesProject.Controllers
{
    [ApiController]
    [Route("api/recipe")]
    public class RecipeController : ControllerBase
    {
        private readonly RecipesDbContext _context;
        private readonly IMapper _mapper;
        private readonly IRecipeService _recipeService;

        public RecipeController(RecipesDbContext context, IMapper mapper, IRecipeService recipeService)
        {
            _context = context;
            _mapper = mapper;
            _recipeService = recipeService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<RecipeDto>> GetAll()
        {
            var recipes = _recipeService.GetAll();

            return Ok(recipes);
        }
        [HttpGet("{id:int}")]
        public ActionResult<RecipeDto> Get([FromRoute] int id)
        {
            var recipe = _recipeService.GetById(id);

            return Ok(recipe);
        }
        [HttpPost]
        public ActionResult Post([FromBody] CreateRecipeDto recipeParam)
        {
            var recipeId = _recipeService.CreateRecipe(recipeParam);

            if (recipeId != 0)
                return Created($"api/recipe/{recipeId}", null);
            else
                return BadRequest("Something went wrong while saving data");
        }

        [HttpPut("{id:int}")]
        public IActionResult Put([FromRoute] int id, [FromBody] CreateRecipeDto recipeParam)
        {
            _recipeService.EditRecipe(id, recipeParam);

            return Ok();
        }

        [HttpDelete("{id:int}")]
        public IActionResult Delete([FromRoute] int id)
        {
            _recipeService.Delete(id);

            return NoContent();
        }
    }
}
