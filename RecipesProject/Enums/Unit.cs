﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RecipesProject.Enums
{
    public enum Unit
    {
        [Display(Name = "gram")]
        g,
        [Display(Name = "kg")]
        kg,
        [Display(Name = "ml")]
        ml,
        [Display(Name = "litr")]
        l,
        [Display(Name = "szt")]
        piece
    }
}
