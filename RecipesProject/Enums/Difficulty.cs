﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecipesProject.Enums
{
	public enum Difficulty
	{
		Easy = 1,
		Medium = 2,
		Hard = 3
	}
}
